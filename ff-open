#!/bin/sh
#
# A short script to pull environment variables from Firefox so I can open tabs via SSH
#
# usage: `ff-send "$URL"`

# some variables for sending URLs to be opened on a remote device
# set them to an empty string if you don't want this
low_spec_host='Theseus'
target_host='Lich.local'

debug=false
if [ "$debug" = 'true' ]; then
	set -x

	if [ -z "$XDG_CACHE_HOME" ]; then
		export XDG_CACHE_HOME="${HOME}/.cache/"
	fi

	#{ log everything to $XDG_CACHE_HOME/ff-open.log
	exec 3>&1 4>&2
	trap 'exec 2>&4 1>&3' 0 1 2 3
	exec 1> "${XDG_CACHE_HOME}/ff-open.log" 2>&1
	#}
fi

if [ "$#" -gt 0 ]; then
	if [ "$#" -gt 1 ]; then
		# flag for internal use
		if [ "$#" -eq 2 ] && [ "$1" = '-b64' ]; then
			# firefox doesn't like being passed an empty string
			if [ "${2:-z}" = 'z' ]; then
				printf 'Invalid argument\n' 1>&2
				exit 11
			else
				url="$(echo "$2" | base64 -d -)"
			fi
		else
			printf 'Multiple arguments are unsupported\n' 1>&2
			printf 'Only using the first argument\n' 1>&2
		fi
	else
		# firefox doesn't like being passed an empty string
		if [ "${1:-z}" = 'z' ]; then
			noargs=true
		else
			noargs=false
			url="$1"
		fi
	fi
else
	noargs=true
fi

if command -v pueue >/dev/null; then
	if ! pueue group | grep 'Group "ff-open"' >/dev/null; then
		pueue group add -p 2 ff-open
	fi
	has_pueue=true
else
	has_pueue=false
fi

# Note: This assumes each instance creates it's own process group
get_process_groups() {
	# shellcheck disable=SC2046
	ps --no-headers -o 'pgid' $(pgrep --uid "$(id -u)" --full --exact '/usr/lib/firefox/firefox.+') | sort -u | tr -d ' '
}

number_of_firefox_instances() {
	# TODO: figure out what Firefox uses to decide if an instance is already running
	# discard the results because I can't use arrays here
	if ! pgrep --uid "$(id -u)" --full --exact '/usr/lib/firefox/firefox.+' > /dev/null; then
		printf '0\n'
	else
		# shellcheck disable=SC2046
		number_of_pgroups=$(get_process_groups | wc -l)
		printf '%s\n' "$number_of_pgroups"
	fi
}

get_newest_process_group() {
get_process_groups |
	(
		while read -r pgroup; do
			# assume PIDs and PGIDs increment endlessly
			if [ "${pgroup:-z}" != 'z' ]; then
				newest_pgroup="$pgroup"
			elif [ "$pgroup" -gt "$newest_pgroup" ]; then
				newest_pgroup="$pgroup"
			fi
		done
		# dance around being in a subshell
		echo "$newest_pgroup"
	)
}

pueue_add() {
	exec pueue add --immediate --group ff-open -- "$@"
}

case $(number_of_firefox_instances) in
	0)
		# Take advantage of the setup done here to open tabs remotely when I'm likely to want it
		if [ "$(uname -n)" = "$low_spec_host" ] && [ "$noargs" = 'false' ]; then
			# `avahi-resolve-host-name` doesn't set it's exit code on failure
			# so grep for something resembling an IP address
			# start by checking if there's already multiplexed ssh connection to $target_host
			if ssh -O check "$target_host" || ( avahi-resolve-host-name "$target_host" 2>&1 | \
				grep -Ee '([[:alnum:]]{0,4}:){2,7}[[:alnum:]]{1,4}|([[:digit:]]{1,3}\.){3}[[:digit:]]{1,3}' ) ; then
				# see FIXME below
				if ssh "$target_host" pgrep firefox > /dev/null; then
					if [ "$has_pueue" = 'true' ]; then
						pueue_add ssh "$target_host" ff-open -b64 \'"$(echo "$url" | base64 --wrap=0 -)"\'
					else
						(sh -ec "$(cat <<- EOF | sed -E 's/#.+$//' | tr -s '[:space:]' | tr '\n' ';'
							ssh "$target_host" ff-open "$(echo "$url" | base64 --wrap=0 -)" > "${HOME}/.cache/ff-open.log" 2>&1
						EOF
						)") &
						exit 0
					fi
				fi
			fi
		fi

		printf 'Firefox is not running!\n' 1>&2
		# FIXME: spawning a new instance from SSH doesn't work because there are variables missing
		if [ "${SSH_TTY:-z}" != 'z' ]; then
			printf 'Dying!\n' 1>&2
			exit 10
		else
			printf 'Launching a new instance!\n' 1>&2
		fi
		if [ "$has_pueue" = 'true' ]; then
			if [ "$noargs" = 'true' ]; then
				pueue_add sh -ec "$(cat <<- EOF | sed -E 's/#.+$//' | tr -s '[:space:]' | tr '\n' ';'
					ulimit -c unlimited # we want coredumps if Firefox segfaults
					/usr/bin/firefox --new-instance
				EOF
				)"
			else
				pueue_add sh -ec "$(cat <<- EOF | sed -E 's/#.+$//' | tr -s '[:space:]' | tr '\n' ';'
					ulimit -c unlimited # we want coredumps if Firefox segfaults
					/usr/bin/firefox --new-instance "$url"
				EOF
				)"
			fi
			exit 0
		else
			# run in a backgrounded subshell & exit the main thread
			if [ "$noargs" = 'true' ]; then
				(sh -ec "$(cat <<- EOF | sed -E 's/#.+$//' | tr -s '[:space:]' | tr '\n' ';'
					ulimit -c unlimited # we want coredumps if Firefox segfaults
					/usr/bin/firefox --new-instance > "${HOME}/.cache/firefox.log" 2>&1
				EOF
				)") &
			else
				(sh -ec "$(cat <<- EOF | sed -E 's/#.+$//' | tr -s '[:space:]' | tr '\n' ';'
					ulimit -c unlimited # we want coredumps if Firefox segfaults
					/usr/bin/firefox --new-instance "$url" > "${HOME}/.cache/firefox.log" 2>&1
				EOF
				)") &
			fi
			exit 0
		fi
		;;
	1)
		firefox_pid=$(pgrep --oldest --pgroup "$(get_process_groups)")
		;;
	*)
		# untested with >2 instances
		# FIXME: try to figure out which profile to use so Firefox doesn't ask
		printf 'Multiple instances of Firefox running!\n' 1>&2
		printf 'Using newest one!\n' 1>&2
		firefox_pid=$(pgrep --oldest --pgroup "$(get_newest_process_group)")
		;;
esac

# read & export all variables
# use the Unit Separator (0x1F) as a delimiter because some of my env variables contain newlines
eval "$(export IFS=''; for line in $(< "/proc/$(pgrep -o firefox)/environ" tr '\0' '') ; do
	if [ "$line" != '' ]; then
		printf 'export "%s"\n' "$line"
	fi
done)"

if [ "$has_pueue" = 'true' ]; then
	if [ "$noargs" = 'true' ]; then
		pueue_add firefox --new-tab
	else
		pueue_add firefox --new-tab \'"$url"\'
	fi
else
	if [ "$noargs" = 'true' ]; then
		exec firefox --new-tab
	else
		exec firefox --new-tab \'"$url"\'
	fi
fi
