#!/bin/dash -eu
#

cache_dir="$HOME/.cache/JOSM.sh"

if ! [ -d "$cache_dir" ]; then
	mkdir "$cache_dir"
fi

curl --etag-save "${cache_dir}/etag" --etag-compare "${cache_dir}/etag" --output "${cache_dir}/josm-tested.jar" 'https://josm.openstreetmap.de/josm-tested.jar'

if ! [ -e "${cache_dir}/josm-tested.jar" ]; then
	curl --etag-save "${cache_dir}/etag" --output "${cache_dir}/josm-tested.jar" 'https://josm.openstreetmap.de/josm-tested.jar'
fi

java \
	--add-exports=java.base/sun.security.action=ALL-UNNAMED \
	--add-exports=java.desktop/com.sun.imageio.plugins.jpeg=ALL-UNNAMED \
	--add-exports=java.desktop/com.sun.imageio.spi=ALL-UNNAMED \
	-jar "${cache_dir}/josm-tested.jar"
