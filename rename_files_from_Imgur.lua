#!/usr/bin/lua5.1

local lapp = require('pl.lapp') -- https://lunarmodules.github.io/Penlight/libraries/pl.lapp.html
local pldir = require('pl.dir') -- https://lunarmodules.github.io/Penlight/libraries/pl.dir.html
local plpath = require('pl.path') -- https://lunarmodules.github.io/Penlight/libraries/pl.path.html
local tablex = require('pl.tablex') -- https://lunarmodules.github.io/Penlight/libraries/pl.tablex.html
local stringx = require('pl.stringx') -- https://lunarmodules.github.io/Penlight/libraries/pl.stringx.html
local utils = require('pl.utils') -- https://lunarmodules.github.io/Penlight/libraries/pl.utils.html
local rex = require('rex_pcre2') -- https://rrthomas.github.io/lrexlib/manual.html
local gcrypt = require('luagcrypt') -- https://github.com/Lekensteyn/luagcrypt
local pfcntl = require('posix.fcntl') -- https://luaposix.github.io/luaposix/modules/posix.fcntl.html
local pstat = require('posix.sys.stat') -- https://luaposix.github.io/luaposix/modules/posix.sys.stat.html
local putime = require('posix.utime') -- https://luaposix.github.io/luaposix/modules/posix.utime.html

-- TODO: add stricter argument checking:
--	https://lunarmodules.github.io/Penlight/libraries/pl.lapp.html#add_type
--	https://lunarmodules.github.io/Penlight/libraries/pl.path.html
local args = lapp [[
Looks for files matching Imgur's naming scheme and replaces the name with a SHA256 hash
  -i,--input... (string) Directories containing files for processing
  -o,--output (string) Directory files should be moved to after renaming
  --ignore... (optional string)  Directories to ignore
  <search...> (optional string)  Paths to check for duplicates (not yet implemented)
]]

-- TODO: normalize paths
--[[
-- need to string-ify tables?
args.input = plpath.normpath(args.input)
args.output = plpath.normpath(args.output)
]]--

-- take a lock: https://luaposix.github.io/luaposix/examples/lock.lua.html
local lock_fd, lock_table
do
	-- https://specifications.freedesktop.org/basedir-spec/basedir-spec-0.8.html
	local lock_dir = os.getenv('XDG_RUNTIME_DIR')
	if lock_dir and not plpath.isabs(lock_dir) then
		io.stderr:write('XDG_RUNTIME_DIR does not contain an absolute path, ignoring.')
		lock_dir = nil
	end

	if not lock_dir then
		lock_dir = '/tmp'
	end

	if not plpath.isdir(lock_dir) then
		local success, message = pldir.makepath(lock_dir)
		if not success then
			error(string.format('Directory %s couldn\'t be created: %s', lock_dir, message), 0)
		end
	end

	-- open file
	lock_fd = pfcntl.open(
		lock_dir .. '/rename_files_from_Imgur.lock',
		pfcntl.O_CREAT + pfcntl.O_WRONLY + pfcntl.O_TRUNC,
		pstat.S_IRUSR + pstat.S_IWUSR + pstat.S_IRGRP + pstat.S_IROTH
	)

	-- define lock type
	lock_table = {
	   l_type = pfcntl.F_WRLCK;     -- Exclusive lock
	   l_whence = pfcntl.SEEK_SET;  -- Relative to beginning of file
	   l_start = 0;            -- Start from 1st byte
	   l_len = 0;              -- Lock whole file
	}

	-- take the lock
	if pfcntl.fcntl(lock_fd, pfcntl.F_SETLK, lock_table) == -1 then
	   error('Already running', 0)
	end
end

gcrypt.init()
local regex = rex.new('^([[:alnum:]]{5,7}|MOVEME\\..+)\\..{3,4}$')

-- find candidates to rename
local function find_candidates ( search_dirs, ignore_dirnames )
	local candidates = {}
	for _, search_dir in ipairs(search_dirs) do
		for root, _, files in pldir.walk(search_dir, false) do
			for _, ignore_dirname in ipairs(ignore_dirnames) do
				if stringx.lfind(root, ignore_dirname) then
					tablex.clear(files)
				end
			end
			for _, file in ipairs(files) do
				if regex:match(file) then
					local file_with_path = string.format('%s/%s', root, file)
					local file_size = pstat.stat(file_with_path).st_size
					if file_size > 0 then
						table.insert(candidates, file_with_path)
					end
				end
			end
		end
	end
	return candidates
end

-- rename a candidate
local function rename_candidate ( candidate_path, output_dir )
	-- Convert (hash) bytes to their hexadecimal representation
	local function tohex(s)
		local hex = string.gsub(s, '.', function(c)
			return string.format('%02x', string.byte(c))
		end)
		return hex
	end

	local raw_candidate_hash = gcrypt.Hash(gcrypt.MD_SHA256)
	local candidate_as_string = utils.readfile(candidate_path, true)
	raw_candidate_hash:write(candidate_as_string)
	local candidate_hash = tohex(raw_candidate_hash:read())
	local _, candidate_name_and_ext = plpath.splitpath(candidate_path)
	local _, candidate_ext = plpath.splitext(candidate_name_and_ext)
	candidate_ext = string.lower(candidate_ext)
	if candidate_ext == '.jpeg' then
		candidate_ext = '.jpg'
	end
	local candidate_stat = pstat.stat(candidate_path)
	local candidate_year_modified = os.date('%Y', candidate_stat.st_mtime)
	local candidate_month_modified = os.date('%m', candidate_stat.st_mtime)
	local candidate_output_dir = plpath.join(
		output_dir,
		candidate_year_modified,
		candidate_month_modified
	)
	if not plpath.isdir(candidate_output_dir) then
		local success, message = pldir.makepath(candidate_output_dir)
		if not success then
			error(string.format('Directory %s couldn\'t be created: %s', candidate_output_dir, message), 0)
		end
	end
	local new_candidate_path = plpath.join(
		candidate_output_dir,
		candidate_hash .. candidate_ext
	)
	if plpath.exists(new_candidate_path) then
		print(string.format('%s already exists, updating mtime and deleting candidate', new_candidate_path))
		-- copy modified time from candidate_path to new_candidate_path
		local new_path_stat = pstat.stat(new_candidate_path)
		if candidate_stat.st_mtime > new_path_stat.st_mtime then
			putime.utime(new_candidate_path, candidate_stat.st_mtime)
		end
		os.remove(candidate_path)
	else
		print(string.format('moving %s to %s', candidate_path, new_candidate_path))
		local success = pldir.movefile(candidate_path, new_candidate_path)
		if not success then
			print(string.format('failed to move %s to %s', candidate_path, new_candidate_path))
		end
	end
end

for _, candidate_path in ipairs(find_candidates(args.input, args.ignore)) do
	rename_candidate(candidate_path, args.output)
end

-- release the lock
lock_table.l_type = pfcntl.F_UNLCK
pfcntl.fcntl(lock_fd, pfcntl.F_SETLK, lock_table)
